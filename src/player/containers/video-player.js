import React, { Component } from 'react';
import VideoPlayerLayout from '../components/video-payer-layout';
import Video from '../components/video'
import Title from '../components/title';
import PlayPause from '../components/play-pause';
import Timer from '../components/timer';
import Controls from '../components/video-player-controls';
import ProgressBar from '../components/progress-bar';
import Spinner from '../components/spinner';
import Volume from '../components/volume';
import FullScreen from '../components/full-screen';

class VideoPlayer extends Component {
    state = {
        pause: true,
        duration: 0,
        currentTime: 0,
        loading: false,
    }

    togglePlay = () => {
        this.setState({
            pause: !this.state.pause,
        })
    }

    componentDidMount() {
        this.setState({
            pause: !this.props.autoplay
        })
    }

    handleLoadedMetadata = event => {
        this.video = event.target;
        this.setState({
            duration: this.video.duration,
        })
    }

    hadleTimeUpdate = event => {
        this.setState({
            currentTime: this.video.currentTime,
        })
    }

    handleProgressChange = event => {
        this.video.currentTime = event.target.value;
    }

    handleSeeking = event => {
        this.setState({
            loading: true,
        })
    }

    handleSeeked = event => {
        this.setState({
            loading: false,
        })
    }

    handleVolumeChange = event => {
        this.video.volume = event.target.value;
    }

    handleFullScreenClick = event => {
        if (!document.webkitIsFullScreen) {
            this.player.webkitRequestFullscreen();
        } else {
            document.webkitExitFullscreen();
        }
    }

    setRef = element => {
        this.player = element;
    }

    render() {
        return (
            <VideoPlayerLayout
                setRef={this.setRef}
            >
                <Title
                    title={this.props.title}
                ></Title>
                <Controls>
                    <PlayPause
                        pause={this.state.pause}
                        handleClick={this.togglePlay}
                    ></PlayPause>
                    <Timer
                        duration={this.state.duration}
                        currentTime={this.state.currentTime}
                    ></Timer>
                    <ProgressBar
                        duration={this.state.duration}
                        value={this.state.currentTime}
                        handleProgressChange={this.handleProgressChange}
                        active={this.state.loading}
                    />
                    <Volume
                        handleVolumeChange={this.handleVolumeChange}
                    />
                    <FullScreen
                        handleFullScreenClick={this.handleFullScreenClick}
                    />
                </Controls>
                <Spinner />
                <Video
                    autoplay={this.props.autoplay}
                    pause={this.state.pause}
                    src={this.props.src}
                    handleLoadedMetadata={this.handleLoadedMetadata}
                    hadleTimeUpdate={this.hadleTimeUpdate}
                    handleSeeking={this.handleSeeking}
                    handleSeeked={this.handleSeeked}
                ></Video>
            </VideoPlayerLayout>
        )
    }
}

export default VideoPlayer;